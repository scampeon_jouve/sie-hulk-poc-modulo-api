﻿using System;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using moduloPocApi.Models;
using moduloPocApi.Utils;
using Newtonsoft.Json;

namespace moduloPocApi.Services
{
    public abstract class ServiceHttpClient
    {
        protected ServiceHttpClient(string serviceUrl)
        {
            ServiceUrl = serviceUrl;
        }

        protected string ServiceUrl { get; }

        protected async Task<T> PostServiceRequest<T>(UriBuilder builder, object requestToSerialize = null)
        {
            var request = JsonConvert.SerializeObject(requestToSerialize, GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings);
            return await PostServiceRequest<T>(builder, request).ConfigureAwait(false);
        }
        
        /// <summary>
        ///     Process requests that could return other responses than Json
        ///     Used for requesting to several data provider
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="builder">url builder to the data provider service</param>
        /// <param name="request">as a generic request, it is meant to be in string format</param>
        /// <param name="gzipEnabled">Gzip the request</param>
        /// <returns></returns>
        protected async Task<T> PostServiceRequest<T>(UriBuilder builder, string request, bool gzipEnabled = true)
        {
            try {
                var postRequest = new StringContent(request, Encoding.UTF8, "application/json");
                var serviceResponse = await GetHttpClient(gzipEnabled).PostAsync(builder.ToString(), postRequest).ConfigureAwait(false);
                var response = await ProcessResponse<T>(builder, serviceResponse).ConfigureAwait(false);


                return response;
            }
            catch (Exception e) {
                Console.WriteLine(e);
                throw;
            }
        }


        protected async Task<T> PatchServiceRequest<T>(UriBuilder builder, object requestToSerialize = null, bool gzipEnabled = true)
        {
            var request = JsonConvert.SerializeObject(requestToSerialize, GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings);
            var postRequest = new StringContent(request, Encoding.UTF8, "application/json");
            var serviceResponse = await GetHttpClient(gzipEnabled).SendAsync
                    (new HttpRequestMessage(new HttpMethod("PATCH"), builder.ToString()) {
                        Content = postRequest
                    }).ConfigureAwait(false);

            var response = await ProcessResponse<T>(builder, serviceResponse).ConfigureAwait(false);
            
            return response;
        }
        
        protected async Task<T> GetServiceRequest<T>(UriBuilder builder, bool gzipEnabled = true)
        {
            try
            {
                if (CacheUtils.Contains(builder.ToString()))
                    return (T) CacheUtils.Get(builder.ToString());

                using (var client = GetHttpClient(gzipEnabled)) {
                    var serviceResponse = await client.GetAsync(builder.ToString()).ConfigureAwait(false);
                    
                    var processResponse = await ProcessResponse<T>(builder, serviceResponse).ConfigureAwait(false);
                    CacheUtils.Add(builder.ToString(), processResponse);
                    return processResponse;
                }
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        protected async Task<T> DeleteServiceRequest<T>(UriBuilder builder, bool gzipEnabled = true)
        {
            try {
                using (var client = GetHttpClient(gzipEnabled)) {
                    var serviceResponse = await client.DeleteAsync(builder.ToString()).ConfigureAwait(false);


                    return await ProcessResponse<T>(builder, serviceResponse).ConfigureAwait(false);
                }
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        private async Task<T> ProcessResponse<T>(UriBuilder builder, HttpResponseMessage serviceResponse)
        {
            using (serviceResponse) {
                if (serviceResponse.IsSuccessStatusCode) {
                    dynamic result;
                    if (typeof(T) == typeof(string))
                        result = await serviceResponse.Content.ReadAsStringAsync().ConfigureAwait(false);
                    else
                        result = await serviceResponse.Content.ReadAsAsync<T>(new[] {GlobalConfiguration.Configuration.Formatters.JsonFormatter}).ConfigureAwait(false);

                    return result;
                }

                var intelligibleError = string.Empty;
                Stream stream = null;

                try {
                    stream = await serviceResponse.Content.ReadAsStreamAsync().ConfigureAwait(false);


                    // on essaie de caster le stream en JsonREsultAPI , cas very specific des services hulk ...
                    using (var sr = new StreamReader(stream)) {
                        using (var reader = new JsonTextReader(sr)) {
                            var serializer = JsonSerializer.Create(GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings);
                            var data = serializer.Deserialize<JsonResultApi>(reader);


                            intelligibleError = data.Message;
                        }
                    }
                }
                catch (Exception) {
                    if (stream != null)
                        using (var src = new StreamReader(stream)) {
                            intelligibleError = src.ReadToEnd();
                        }
                }
                finally {
                    stream?.Dispose();
                }


                throw new HttpException((int) serviceResponse.StatusCode, $"{builder.Uri} is not correctly reachable, {serviceResponse.ReasonPhrase}. Inner exception :{intelligibleError}");
            }
        }

        protected virtual HttpClient GetHttpClient(bool gzipEnabled = true)
        {
            return HulkHttpClient.Build(gzipEnabled);
        }
    }
}