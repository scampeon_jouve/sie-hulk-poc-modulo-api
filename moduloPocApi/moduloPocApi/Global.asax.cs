﻿using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using moduloPocApi.App_Start;
using moduloPocApi.Utils;

namespace moduloPocApi
{
    /// <summary>
    ///     Entry point of the application
    /// </summary>
    public class Global : HttpApplication
    {
        /// <summary>
        ///     Raised when the application starts
        /// </summary>
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            SerializerConfig.Register();

            GlobalConfiguration.Configure(WebApiConfig.Register);
            GlobalConfiguration.Configure(SwaggerConfig.Register);

            RouteConfig.RegisterRoutes(RouteTable.Routes);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);

            new ElmahHulkLogger().Info("poc.MODULO service has successfully started");
        }
    }
}