﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using moduloPocApi.Models;
using moduloPocApi.Utils;
using Newtonsoft.Json;
using Search.Model.V2.Domain.Response;

namespace moduloPocApi.Managers
{
    public class SearchManager
    {
        public bool TryCreateConfiguredInstance(string id = null, string file = null)
        {
            var instanceId = string.IsNullOrEmpty(id) ? CommonConfigUtil.SearchApiInstanceId : id;
            var instanceFile = string.IsNullOrEmpty(file) ? CommonConfigUtil.SearchApiInstanceFile : file;

            var resourceStream = Assembly.GetExecutingAssembly().GetManifestResourceStream(string.Concat("moduloPocApi.Conf.", instanceFile));
            if (resourceStream == null)
                return false;

            // Prepare the request URL
            var url = new Uri(new Uri(CommonConfigUtil.SearchApiUrl), $"/instance/{instanceId}");
            var httpClient = HulkHttpClient.Build();

            var queryResult = httpClient.GetAsync(url).Result;
            if (queryResult.StatusCode == HttpStatusCode.OK)
                return true;

            // unexisting CONF in Search API
            new ElmahHulkLogger().Info($"The configured instance {instanceId} from {instanceFile} does not exist");

            var instanceConf = string.Empty;
            try {
                var resourceStreamReader = new StreamReader(resourceStream, Encoding.UTF8);
                instanceConf = resourceStreamReader.ReadToEnd();
            }
            catch (Exception e) {
                new ElmahHulkLogger().Error(e);
            }

            // create the CONF in Search API
            new ElmahHulkLogger().Info($"The configured instance {instanceId} from {instanceFile} will be created");

            url = new Uri(new Uri(CommonConfigUtil.SearchApiUrl), "/instance");
            var content = new StringContent(instanceConf, Encoding.UTF8, "application/json");
            queryResult = httpClient.PostAsync(url, content).Result;

            return queryResult.StatusCode == HttpStatusCode.Created;
        }

        public SearchResult Search(string instanceId, SearchEngineParameters query)
        {
            // Prepare the request URL
            var url = new Uri(new Uri(CommonConfigUtil.SearchApiUrl), $"/search/instance/{instanceId}/results");
            var httpClient = HulkHttpClient.Build();

            var queryJson = string.Empty;
            try {
                queryJson = JsonConvert.SerializeObject(query);
            }
            catch (Exception e) {
                new ElmahHulkLogger().Error(e);
            }

            new ElmahHulkLogger().Error(queryJson);

            var content = new StringContent(queryJson, Encoding.UTF8, "application/json");
            var queryResult = httpClient.PostAsync(url, content).Result;

            if (queryResult.StatusCode == HttpStatusCode.OK)
                return JsonConvert.DeserializeObject<SearchResult>(queryResult.Content.ReadAsStringAsync().Result);
            return null;
        }
    }
}