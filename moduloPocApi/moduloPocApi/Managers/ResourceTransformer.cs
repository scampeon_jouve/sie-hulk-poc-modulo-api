﻿using System;
using System.IO;
using System.Reflection;
using System.Text;
using System.Xml;
using moduloPocApi.Utils;
using Saxon.Api;
using System.Net;
using moduloPocApi.Models;

namespace moduloPocApi.Managers
{
    public class ResourceTransformer
    {
        public const string XslXmlContentToXhtml = "xml-content-to-xhtml.xsl";
        private readonly string _xslContent;

        public ResourceTransformer()
        {
            var resourceStream = Assembly.GetExecutingAssembly().GetManifestResourceStream(string.Concat("moduloPocApi.Xsl.", XslXmlContentToXhtml));
            if (resourceStream == null)
                return;

            // get the XSL content
            var resourceStreamReader = new StreamReader(resourceStream, Encoding.UTF8);
            _xslContent = resourceStreamReader.ReadToEnd();
        }

        public string ProcessResource(string rawContent, Relation[] relations = null)
        {
            if (rawContent == null)
                return null;
            if (_xslContent == null)
                return rawContent;

            try
            {
                var crUriResolver = new CrUriResolver(relations);

                // Create a Processor instance.
                var processor = new Processor();

                var input = processor.NewDocumentBuilder().Build(XmlReader.Create(new StringReader(rawContent)));
                processor.XmlResolver = crUriResolver;
                var compiler = processor.NewXsltCompiler();

                // Not required but we need to set otherwise exception will be thrown
                compiler.BaseUri = new Uri("cr:"); // FIXME !!

                // Create a transformer for the stylesheet.
                var transformer = compiler.Compile(XmlReader.Create(new StringReader(_xslContent))).Load();
                transformer.InitialContextNode = input;
                transformer.InputXmlResolver = crUriResolver;

                // Create a serializer.
                var serializer = processor.NewSerializer();
                serializer.SetOutputProperty(Serializer.INDENT, "no");
                serializer.SetOutputProperty(Serializer.SUPPRESS_INDENTATION, "yes");

                var output = new MemoryStream();
                serializer.SetOutputStream(output);

                // Transform the source XML
                transformer.Run(serializer);

                output.Position = 0;
                var sr = new StreamReader(output);
                return sr.ReadToEnd();
            }
            catch (Exception e)
            {
                new ElmahHulkLogger().Error(e);
            }

            return rawContent;
        }
    }

    public class CrUriResolver : XmlResolver
    {
        private Relation[] _relations;

        public CrUriResolver(Relation[] relations)
        {
            _relations = relations;
        }

        public override ICredentials Credentials
        {
            set { }
        }

        public override object GetEntity(Uri absoluteUri, string role, Type ofObjectToReturn)
        {
            MemoryStream entityStream = null;

            switch (absoluteUri.Scheme)
            {
                case "cr":

                    var absoluteUriOriginalString = absoluteUri.OriginalString;
                    var crEntityId = absoluteUriOriginalString.Substring(absoluteUriOriginalString.IndexOf(":") + 1);
                    var crIdResolution = crEntityId;

                    if (!string.IsNullOrEmpty(crEntityId) && _relations != null)
                    {
                        foreach (var relation in _relations)
                        {
                            if (relation.Target != null && relation.Target.AltKeys != null)
                            {
                                foreach (var altKey in relation.Target.AltKeys)
                                {
                                    if (altKey.Equals(crEntityId))
                                    {
                                        var apiId = relation.Target.Identifier.Replace("/publishers/", "").Replace("/documents/", ":").Replace("/languages/fr", "");
                                        crIdResolution = $"<target title=\"{relation.Target.Title}\" content-type='{relation.Target.ContentType.ToString()}' relation-type='{relation.RelationType.ToString()}' id='{apiId}'/>";
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    
                    var utf8Encoding = new UTF8Encoding();
                    var entityBytes = utf8Encoding.GetBytes(crIdResolution);
                    entityStream = new MemoryStream(entityBytes);

                    break;
            }

            return entityStream;
        }

        public override Uri ResolveUri(Uri baseUri, string relativeUri)
        {
            // You might want to resolve all reference URIs using a custom scheme.
            if (baseUri != null)
                return base.ResolveUri(baseUri, relativeUri);

            return new Uri("cr:" + relativeUri);
        }
    }
}