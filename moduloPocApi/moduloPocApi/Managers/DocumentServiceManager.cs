﻿using System.Collections.Generic;
using System.Threading.Tasks;
using moduloPocApi.Models;
using moduloPocApi.Models.Entities;
using moduloPocApi.Providers;

namespace moduloPocApi.Managers
{
    public class DocumentServiceManager
    {
        public async Task<string> GetDocumentItem(string url)
        {
            var dataProvider = new DataProviderClient(url);
            var itemContent = await dataProvider.GetRawContent().ConfigureAwait(false);

            return itemContent;
        }

        public async Task<ExpressionResult> GetDocument(string url)
        {
            var dataProvider = new DataProviderClient(url);
            var expressionResult = await dataProvider.GetContent().ConfigureAwait(false);

            // To suppress in API result the Relations
            expressionResult.Relations = null;

            return expressionResult;
        }

        /// <summary>
        ///     Getting all publishers
        /// </summary>
        /// <param name="url">The URL to get the publishers</param>
        /// <returns>The publishers</returns>
        public async Task<IEnumerable<PublisherEntity>> GetPublishers(string url)
        {
            var dataProvider = new DataProviderClient(url);
            var result = await dataProvider.GetPublishers().ConfigureAwait(false);
            return result;
        }
    }
}