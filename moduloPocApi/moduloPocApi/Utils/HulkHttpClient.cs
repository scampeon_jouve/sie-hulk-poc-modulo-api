﻿using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;

namespace moduloPocApi.Utils
{
    public class HulkHttpClient
    {
        // TLB : suite à bug survenu sur la plateforme de test, on s'est apperçu qu'il ne fallait pas
        // systématiser l'utilisation d'un proxy dans cette méthode. On s'est donc posé la question de comment
        // faire transiter l'information d'utilisation d'un proxy, et il a été convenu d'ajouter l'ainformation dans 
        // les urls stockées dans l'appsettings sous la forme http://monurl.com|proxy
        // Or, la classe HttpClient ne prends pas en parametre d'url, ce qui fait qu'on perds la factorisation de
        // ce découpage d'url, et que chaque utilisateur de cette classe doit donc découper l'url pour en extraire 
        // l'éventuel info de |proxy. Je ne suis pas certain de l'interet de cette façon de faire. Une US a été créée 
        // pour corriger ce soucis et en attendant je patche le code rapidement en passant un nouveau param
        // pour respecter les calendriers de MEP.
        public static HttpClient Build(bool gzipEnabled = true, string apiKeyToken = null, bool useProxy = false)
        {
            var httpClientHandler = new HttpClientHandler();
            
            if (CommonConfigUtil.AddressProxy != null)
            {
                httpClientHandler.Proxy = new WebProxy(CommonConfigUtil.AddressProxy);
                httpClientHandler.UseProxy = useProxy;
            }

            if (gzipEnabled)
                httpClientHandler.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            
            HttpMessageHandler httpMessageHandler = httpClientHandler;
            
            //if (gzipEnabled)
            //    httpMessageHandler = new ClientCompressionHandler(new GZipCompressor(), new DeflateCompressor()) {InnerHandler = httpMessageHandler};
            
            var client = new HttpClient(httpMessageHandler);
            
            if (gzipEnabled) {
                client.DefaultRequestHeaders.AcceptEncoding.Add(new StringWithQualityHeaderValue("gzip"));
                client.DefaultRequestHeaders.AcceptEncoding.Add(new StringWithQualityHeaderValue("deflate"));
            }
            
            if (!string.IsNullOrEmpty(apiKeyToken))
                client.DefaultRequestHeaders.Add("x-api-key", apiKeyToken);
            
            return client;
        }
    }
}