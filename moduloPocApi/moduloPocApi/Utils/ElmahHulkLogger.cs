﻿using System;
using Elmah;
using ApplicationException = System.ApplicationException;

namespace moduloPocApi.Utils
{
    public class ElmahHulkLogger
    {
        public bool ActivateInfo { get; set; }

        // Par defaut, on ne log pas les Trace dans Elmah
        public bool ActivateTrace { get; set; }

        public ElmahHulkLogger(bool activateInfo = true, bool activateTrace = false)
        {
            ActivateInfo = activateInfo;
            ActivateTrace = activateTrace;
        }

        private static void LogMessage(string message)
        {
            try {
                ErrorSignal.FromCurrentContext().Raise(new ApplicationException(message));
            }
            catch (Exception) {
                try {
                    ErrorLog.GetDefault(null).Log(new Error(new ApplicationException(message)));
                }
                catch {
                    // Volontairement vide car on a pas toujours de context Elmah lorsque l'on log (ex: initalisation app)
                }
            }
        }

        private static void LogException(string message, Exception exception)
        {
            try {
                ErrorSignal.FromCurrentContext().Raise(message == null ? exception : new ApplicationException(message, exception));
            }
            catch {
                try {
                    ErrorLog.GetDefault(null).Log(new Error(message == null ? exception : new ApplicationException(message, exception)));
                }
                catch {
                    // Volontairement vide car on a pas toujours de context Elmah lorsque l'on log (ex: initalisation app)
                }
            }
        }

        /// <summary>
        ///     Log un message d'erreur
        /// </summary>
        /// <param name="message">Le message à logger</param>
        public void Error(string message)
        {
            LogMessage(message);
        }

        /// <summary>
        ///     Log un message d'avertissement
        /// </summary>
        /// <param name="message">Le message à logger</param>
        public void Warning(string message)
        {
            LogMessage(message);
        }

        /// <summary>
        ///     Log un message d'information
        /// </summary>
        /// <param name="message">Le message à logger</param>
        public void Info(string message)
        {
            if (ActivateInfo)
                LogMessage(message);
        }

        public void Trace(string message)
        {
            if (ActivateTrace)
                LogMessage(message);
        }

        /// <summary>
        ///     Log un message d'erreur
        /// </summary>
        /// <param name="exception">L'exception à logger</param>
        /// <param name="message">Le message complémentaire à logger</param>
        public void Error(Exception exception, string message = null)
        {
            LogException(message, exception);
        }

        /// <summary>
        ///     Log un message d'avertissement
        /// </summary>
        /// <param name="exception">L'exception à logger</param>
        /// <param name="message">Le message complémentaire à logger</param>
        public void Warning(Exception exception, string message = null)
        {
            LogException(message, exception);
        }

        /// <summary>
        ///     Log un message d'information
        /// </summary>
        /// <param name="exception">L'exception à logger</param>
        /// <param name="message">Le message complémentaire à logger</param>
        public void Info(Exception exception, string message = null)
        {
            if (ActivateInfo)
                LogException(message, exception);
        }
    }
}