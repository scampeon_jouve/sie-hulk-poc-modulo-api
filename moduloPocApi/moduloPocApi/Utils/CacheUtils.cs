﻿using System;
using System.Runtime.Caching;

namespace moduloPocApi.Utils
{
    /// <summary>
    /// Basic Memory cache
    /// </summary>
    public static class CacheUtils
    {
        private static readonly MemoryCache Cache = MemoryCache.Default;
        private static readonly CacheItemPolicy Policy = new CacheItemPolicy { SlidingExpiration = new TimeSpan(0, 15, 0) };

        /// <summary>
        /// Add key, value in cache
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public static void Add(string key, object value)
        {
            Cache.Add(key, value, Policy);
        }

        /// <summary>
        /// Get value by key in cache
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static object Get(string key)
        {
            return Cache.Get(key);
        }

        /// <summary>
        /// Is key in cache
        /// </summary>
        /// <param name="key"></param>
        public static bool Contains(string key)
        {
            return Cache.Contains(key);
        }
    }
}