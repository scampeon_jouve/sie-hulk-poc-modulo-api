﻿using System;
using System.Configuration;

namespace moduloPocApi.Utils
{
    public static class CommonConfigUtil
    {
        /// <summary>
        ///     Configuration du proxy HTTP pour les requetes sortantes de l'application. OPTIONNEL
        /// </summary>
        public static Uri AddressProxy {
            get {
#if DEBUG
                return null;
#endif
                var value = ConfigurationManager.AppSettings["AddressProxy"];
                return string.IsNullOrEmpty(value) ? null : new Uri(value);
            }
        }

        public static string AllowedOrigins {
            get {
                var value = ConfigurationManager.AppSettings["AllowedOrigins"];
                return string.IsNullOrEmpty(value) ? "*" : value;
            }
        }

        public static string ContentRepoUrl {
            get {
                var value = ConfigurationManager.AppSettings["ContentRepoUrl"];
                return string.IsNullOrEmpty(value) ? "https://contentrepo-api-main.larciergroup-aws-preprod.com" : value;
            }
        }

        public static string SearchApiUrl {
            get {
                var value = ConfigurationManager.AppSettings["SearchApiUrl"];
                return string.IsNullOrEmpty(value) ? "http://search-service.fr000-tstapp046.elsapp.local/api/v2/" : value;
            }
        }

        public static string SearchApiInstanceId {
            get {
                var value = ConfigurationManager.AppSettings["SearchApiInstanceId"];
                return string.IsNullOrEmpty(value) ? "MODULO" : value;
            }
        }

        public static string SearchApiInstanceFile {
            get {
                var value = ConfigurationManager.AppSettings["SearchApiInstanceFile"];
                return string.IsNullOrEmpty(value) ? "instance-modulo.json" : value;
            }
        }
    }
}