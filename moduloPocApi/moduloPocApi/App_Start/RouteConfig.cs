﻿using System.Web.Mvc;
using System.Web.Routing;

namespace moduloPocApi.App_Start
{
    /// <summary>
    ///     Routing management
    /// </summary>
    public class RouteConfig
    {
        /// <summary>
        ///     Register the routes
        /// </summary>
        /// <param name="routes">the routes</param>
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("elmah.axd");
            routes.IgnoreRoute("elmah");

            routes.MapRoute("Default", "{controller}/{action}/{id}", new {controller = "Index", action = "Index", id = UrlParameter.Optional});
        }
    }
}