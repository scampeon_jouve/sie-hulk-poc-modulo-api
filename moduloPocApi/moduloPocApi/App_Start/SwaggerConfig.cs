﻿using System.Web.Http;
using System.Web.Routing;
using NSwag.AspNet.Owin;
using NJsonSchema;
using NSwag;

namespace moduloPocApi.App_Start
{
    /// <summary>
    ///     Swagger management
    /// </summary>
    public class SwaggerConfig
    {
        /// <summary>
        ///     Register the Swagger service
        /// </summary>
        /// <param name="config">the HTTP configuration</param>
        public static void Register(HttpConfiguration config)
        {
            RouteTable.Routes.MapOwinPath("swagger", app => {
                app.UseSwaggerUi3
                (typeof(Global).Assembly, settings =>
                {
                    settings.Path = "/swagger";
                    settings.DocumentPath = "/openapi/v1/openapi.json";
                    settings.MiddlewareBasePath = "/swagger";
                    settings.GeneratorSettings.DefaultUrlTemplate = "api/{controller}/{id}";
                    settings.GeneratorSettings.SchemaType = SchemaType.OpenApi3;
                    settings.GeneratorSettings.SerializerSettings = GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings;
                    settings.PostProcess = SetDocumentInfo;
                });
            });

            RouteTable.Routes.MapOwinPath("redocer", app => {
                app.UseSwaggerReDoc(typeof(Global).Assembly, settings =>
                {
                    settings.Path = "/redocer";
                    settings.DocumentPath = "/openapi/v1/openapi.json";
                    settings.MiddlewareBasePath = "/redocer";
                    settings.GeneratorSettings.DefaultUrlTemplate = "api/{controller}/{id}";
                    settings.GeneratorSettings.SchemaType = SchemaType.OpenApi3;
                    settings.GeneratorSettings.SerializerSettings = GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings;
                    settings.PostProcess = SetDocumentInfo;
                });
            });
        }

        private static void SetDocumentInfo(SwaggerDocument document)
        {
            document.Info.Version = "v1";
            document.Info.Title = "poc.MODULO API";
            document.Info.Description = "The poc.MODULO API allows consumers to request content from Content Repository.";
            document.Info.TermsOfService = "None";
        }
    }
}