﻿using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.ExceptionHandling;
using System.Web.Http.Routing;
using Elmah.Contrib.WebApi;
using Microsoft.Web.Http;
using Microsoft.Web.Http.Routing;
using Microsoft.Web.Http.Versioning;

namespace moduloPocApi.App_Start
{
    /// <summary>
    ///     Configuration and Web API management
    /// </summary>
    public static class WebApiConfig
    {
        /// <summary>
        ///     Register the configuration and the Web API services
        /// </summary>
        /// <param name="config">the HTTP configuration</param>
        public static void Register(HttpConfiguration config)
        {
            // configuration and Web API services
            config.AddApiVersioning
                    (
                            o => {
                                o.ReportApiVersions = true;
                                o.ApiVersionReader = new UrlSegmentApiVersionReader();
                                o.AssumeDefaultVersionWhenUnspecified = true;
                                o.DefaultApiVersion = new ApiVersion(1, 0);
                            });

            var constraintResolver = new DefaultInlineConstraintResolver
            {
                ConstraintMap = { ["apiVersion"] = typeof(ApiVersionRouteConstraint) }
            };

            var customRoutesProvider = new WebApiCustomDirectRouteProvider();
            config.MapHttpAttributeRoutes(constraintResolver, customRoutesProvider);
            config.Services.Add(typeof(IExceptionLogger), new ElmahExceptionLogger());
        }

        /// <summary>
        ///     Redefine the default route
        /// </summary>
        public class WebApiCustomDirectRouteProvider : DefaultDirectRouteProvider
        {
            /// <summary>
            ///     Inherit route attributes decorated on base class controller's actions
            /// </summary>
            /// <param name="actionDescriptor">information about the action method</param>
            /// <returns>the route attributes decorated on base class controller's actions</returns>
            protected override IReadOnlyList<IDirectRouteFactory> GetActionRouteFactories(HttpActionDescriptor actionDescriptor)
            {
                return actionDescriptor.GetCustomAttributes<IDirectRouteFactory>(true);
            }
        }
    }
}