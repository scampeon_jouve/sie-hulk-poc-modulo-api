﻿using System.Web.Mvc;

namespace moduloPocApi.App_Start
{
    /// <summary>
    ///     Allows to manage the global filters
    /// </summary>
    public class FilterConfig
    {
        /// <summary>
        ///     Register the global filters
        /// </summary>
        /// <param name="filters">the global filters</param>
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}