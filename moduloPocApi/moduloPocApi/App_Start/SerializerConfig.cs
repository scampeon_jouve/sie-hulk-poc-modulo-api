﻿using System.Web.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace moduloPocApi.App_Start
{
    /// <summary>
    ///     Manage the serializer settings
    /// </summary>
    public static class SerializerConfig
    {
        /// <summary>
        ///     Register the serializer settings
        /// </summary>
        public static void Register()
        {
            var serializerSettings = GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings;
            serializerSettings.TypeNameHandling = TypeNameHandling.Auto;
            serializerSettings.NullValueHandling = NullValueHandling.Ignore;
            serializerSettings.ContractResolver = new DefaultContractResolver {NamingStrategy = new SnakeCaseNamingStrategy()};
        }
    }
}