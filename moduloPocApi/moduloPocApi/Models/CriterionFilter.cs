﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace moduloPocApi.Models
{
    /// <summary>
    ///     Criterion filter, to perform a search filtering on a specified criterion filter
    /// </summary>
    [Description("Criterion filter, to perform a search filtering on a specified criterion filter")]
    public class CriterionFilter
    {
        /// <summary>
        ///     field id
        /// </summary>
        [Description("Id of the criterion to filter on")]
        [JsonProperty("criterion-id")]
        public string CriterionId { get; private set; }

        /// <summary>
        ///     Values used by the filter
        /// </summary>
        [Description("Array of string value used by the filter")]
        [JsonProperty("values")]
        public string[] Values { get; private set; }

        /// <inheritdoc />
        /// <summary>
        ///     Default constructor, required for Json/Net's serialization/deserialization processes
        /// </summary>
        public CriterionFilter() { }

        /// <inheritdoc />
        public CriterionFilter(string criterionId, string value)
        {
            CriterionId = criterionId;
            Values = new[] {value};
        }

        /// <inheritdoc />
        public CriterionFilter(string criterionId, string[] values)
        {
            CriterionId = criterionId;
            Values = values;
        }
    }
}