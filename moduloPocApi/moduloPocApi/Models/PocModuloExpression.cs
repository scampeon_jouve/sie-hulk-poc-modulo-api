﻿//    using pocModuloApi.Models.Expression;
//
//    var pocModuloExpression = PocModuloExpression.FromJson(jsonString);

using System;
using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace moduloPocApi.Models
{
    public partial class PocModuloExpression
    {
        [JsonProperty("objectType", NullValueHandling = NullValueHandling.Ignore)]
        public string ObjectType { get; set; }

        [JsonProperty("schemaVersion", NullValueHandling = NullValueHandling.Ignore)]
        public string SchemaVersion { get; set; }

        [JsonProperty("identifier", NullValueHandling = NullValueHandling.Ignore)]
        public string Identifier { get; set; }

        [JsonProperty("altIdentifiers", NullValueHandling = NullValueHandling.Ignore)]
        public Uri[] AltIdentifiers { get; set; }

        [JsonProperty("altKeys", NullValueHandling = NullValueHandling.Ignore)]
        public string[] AltKeys { get; set; }

        [JsonProperty("language", NullValueHandling = NullValueHandling.Ignore)]
        public Language? Language { get; set; }

        [JsonProperty("title", NullValueHandling = NullValueHandling.Ignore)]
        public string Title { get; set; }

        [JsonProperty("alternatives", NullValueHandling = NullValueHandling.Ignore)]
        public Alternative[] Alternatives { get; set; }

        [JsonProperty("documentDate", NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? DocumentDate { get; set; }

        [JsonProperty("dates", NullValueHandling = NullValueHandling.Ignore)]
        public Date[] Dates { get; set; }

        [JsonProperty("contentType", NullValueHandling = NullValueHandling.Ignore)]
        public ContentTypeEnum? ContentType { get; set; }

        [JsonProperty("types", NullValueHandling = NullValueHandling.Ignore)]
        public Uri[] Types { get; set; }

        [JsonProperty("sources", NullValueHandling = NullValueHandling.Ignore)]
        public string[] Sources { get; set; }

        [JsonProperty("documentNumber", NullValueHandling = NullValueHandling.Ignore)]
        public string DocumentNumber { get; set; }

        [JsonProperty("numbers", NullValueHandling = NullValueHandling.Ignore)]
        public Number[] Numbers { get; set; }

        [JsonProperty("isExpressionOfWork", NullValueHandling = NullValueHandling.Ignore)]
        public IsExpressionOfWork IsExpressionOfWork { get; set; }

        [JsonProperty("publisher", NullValueHandling = NullValueHandling.Ignore)]
        public Publisher Publisher { get; set; }

        [JsonProperty("rights", NullValueHandling = NullValueHandling.Ignore)]
        public string Rights { get; set; }

        [JsonProperty("properties", NullValueHandling = NullValueHandling.Ignore)]
        public Property[] Properties { get; set; }

        [JsonProperty("valid", NullValueHandling = NullValueHandling.Ignore)]
        public Valid Valid { get; set; }

        [JsonProperty("relations", NullValueHandling = NullValueHandling.Ignore)]
        public Relation[] Relations { get; set; }

        [JsonProperty("subjects", NullValueHandling = NullValueHandling.Ignore)]
        public Subject[] Subjects { get; set; }

        [JsonProperty("createdAt", NullValueHandling = NullValueHandling.Ignore)]
        public string CreatedAt { get; set; }

        [JsonProperty("creators", NullValueHandling = NullValueHandling.Ignore)]
        public Creator[] Creators { get; set; }

        [JsonProperty("resources", NullValueHandling = NullValueHandling.Ignore)]
        public Resource[] Resources { get; set; }

        [JsonProperty("publishedIn", NullValueHandling = NullValueHandling.Ignore)]
        public PublishedIn[] PublishedIn { get; set; }

        [JsonProperty("hasParts", NullValueHandling = NullValueHandling.Ignore)]
        public HasPart[] HasParts { get; set; }

        [JsonProperty("tableOfContents", NullValueHandling = NullValueHandling.Ignore)]
        public TableOfContents TableOfContents { get; set; }

        [JsonProperty("href", NullValueHandling = NullValueHandling.Ignore)]
        public Uri Href { get; set; }
    }

    public class Alternative
    {
        [JsonProperty("type", NullValueHandling = NullValueHandling.Ignore)]
        public string Type { get; set; }

        [JsonProperty("title", NullValueHandling = NullValueHandling.Ignore)]
        public string Title { get; set; }
    }

    public class Creator
    {
        [JsonProperty("role", NullValueHandling = NullValueHandling.Ignore)]
        public string Role { get; set; }

        [JsonProperty("agent", NullValueHandling = NullValueHandling.Ignore)]
        public Agent Agent { get; set; }
    }

    public class Agent
    {
        [JsonProperty("details", NullValueHandling = NullValueHandling.Ignore)]
        public Subject Details { get; set; }

        [JsonProperty("type", NullValueHandling = NullValueHandling.Ignore)]
        public string Type { get; set; }

        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }

        [JsonProperty("identifier", NullValueHandling = NullValueHandling.Ignore)]
        public string Identifier { get; set; }

        [JsonProperty("href", NullValueHandling = NullValueHandling.Ignore)]
        public Uri Href { get; set; }

        [JsonProperty("title", NullValueHandling = NullValueHandling.Ignore)]
        public string Title { get; set; }

        [JsonProperty("givenName", NullValueHandling = NullValueHandling.Ignore)]
        public string GivenName { get; set; }

        [JsonProperty("familyName", NullValueHandling = NullValueHandling.Ignore)]
        public string FamilyName { get; set; }
    }

    public class HasPart
    {
        [JsonProperty("content_type", NullValueHandling = NullValueHandling.Ignore)]
        public string ContentType { get; set; }

        [JsonProperty("document_date", NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? DocumentDate { get; set; }

        [JsonProperty("types", NullValueHandling = NullValueHandling.Ignore)]
        public string[] Types { get; set; }

        [JsonProperty("target", NullValueHandling = NullValueHandling.Ignore)]
        public Target Target { get; set; }

        [JsonProperty("href", NullValueHandling = NullValueHandling.Ignore)]
        public Uri Href { get; set; }

        [JsonProperty("identifier", NullValueHandling = NullValueHandling.Ignore)]
        public string Identifier { get; set; }
    }

    public class Date
    {
        [JsonProperty("type", NullValueHandling = NullValueHandling.Ignore)]
        public string Type { get; set; }

        [JsonProperty("value", NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? Value { get; set; }

        [JsonProperty("comment", NullValueHandling = NullValueHandling.Ignore)]
        public string Comment { get; set; }
    }

    public class IsExpressionOfWork
    {
        [JsonProperty("identifier", NullValueHandling = NullValueHandling.Ignore)]
        public string Identifier { get; set; }

        [JsonProperty("workExpressedBy", NullValueHandling = NullValueHandling.Ignore)]
        public Publisher[] WorkExpressedBy { get; set; }

        [JsonProperty("href", NullValueHandling = NullValueHandling.Ignore)]
        public Uri Href { get; set; }
    }

    public class Publisher
    {
        [JsonProperty("title", NullValueHandling = NullValueHandling.Ignore)]
        public string Title { get; set; }

        [JsonProperty("language", NullValueHandling = NullValueHandling.Ignore)]
        public Language? Language { get; set; }

        [JsonProperty("href", NullValueHandling = NullValueHandling.Ignore)]
        public Uri Href { get; set; }

        [JsonProperty("identifier", NullValueHandling = NullValueHandling.Ignore)]
        public string Identifier { get; set; }

        [JsonProperty("contentType", NullValueHandling = NullValueHandling.Ignore)]
        public string ContentType { get; set; }
    }

    public class Number
    {
        [JsonProperty("type", NullValueHandling = NullValueHandling.Ignore)]
        public string Type { get; set; }

        [JsonProperty("value", NullValueHandling = NullValueHandling.Ignore)]
        public string Value { get; set; }
    }

    public class Property
    {
        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }

        [JsonProperty("value", NullValueHandling = NullValueHandling.Ignore)]
        public string Value { get; set; }
    }

    public class PublishedIn
    {
        [JsonProperty("issue", NullValueHandling = NullValueHandling.Ignore)]
        public Issue Issue { get; set; }

        [JsonProperty("href", NullValueHandling = NullValueHandling.Ignore)]
        public Uri Href { get; set; }

        [JsonProperty("identifier", NullValueHandling = NullValueHandling.Ignore)]
        public string Identifier { get; set; }
    }

    public class Issue
    {
        [JsonProperty("language", NullValueHandling = NullValueHandling.Ignore)]
        public Language? Language { get; set; }

        [JsonProperty("issueName", NullValueHandling = NullValueHandling.Ignore)]
        public string IssueName { get; set; }

        [JsonProperty("issueType", NullValueHandling = NullValueHandling.Ignore)]
        public string IssueType { get; set; }

        [JsonProperty("identifier", NullValueHandling = NullValueHandling.Ignore)]
        public string Identifier { get; set; }

        [JsonProperty("href", NullValueHandling = NullValueHandling.Ignore)]
        public Uri Href { get; set; }
    }

    public class Relation
    {
        [JsonProperty("target", NullValueHandling = NullValueHandling.Ignore)]
        public Target Target { get; set; }

        [JsonProperty("identifier", NullValueHandling = NullValueHandling.Ignore)]
        public string Identifier { get; set; }

        [JsonProperty("properties", NullValueHandling = NullValueHandling.Ignore)]
        public Properties Properties { get; set; }

        [JsonProperty("relationType", NullValueHandling = NullValueHandling.Ignore)]
        public RelationType? RelationType { get; set; }

        [JsonProperty("href", NullValueHandling = NullValueHandling.Ignore)]
        public Uri Href { get; set; }

        [JsonProperty("part", NullValueHandling = NullValueHandling.Ignore)]
        public Part Part { get; set; }
    }

    public class Part
    {
        [JsonProperty("anchor", NullValueHandling = NullValueHandling.Ignore)]
        public string Anchor { get; set; }

        [JsonProperty("article", NullValueHandling = NullValueHandling.Ignore)]
        public string Article { get; set; }

        [JsonProperty("literal", NullValueHandling = NullValueHandling.Ignore)]
        public string Literal { get; set; }

        [JsonProperty("paragraph", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(ParseStringConverter))]
        public long? Paragraph { get; set; }

        [JsonProperty("alinea", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(ParseStringConverter))]
        public long? Alinea { get; set; }

        [JsonProperty("point", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(ParseStringConverter))]
        public long? Point { get; set; }

        [JsonProperty("annex", NullValueHandling = NullValueHandling.Ignore)]
        public string Annex { get; set; }

        [JsonProperty("section", NullValueHandling = NullValueHandling.Ignore)]
        public string Section { get; set; }

        [JsonProperty("recital", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(ParseStringConverter))]
        public long? Recital { get; set; }
    }

    public class Properties
    {
        [JsonProperty("transposition_notification", NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? TranspositionNotification { get; set; }

        [JsonProperty("transposition_deadline_transmitted", NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? TranspositionDeadlineTransmitted { get; set; }

        [JsonProperty("start_of_validity", NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? StartOfValidity { get; set; }

        [JsonProperty("sub_relation_type", NullValueHandling = NullValueHandling.Ignore)]
        public SubRelationType? SubRelationType { get; set; }

        [JsonProperty("reference_to_modified_location", NullValueHandling = NullValueHandling.Ignore)]
        public string ReferenceToModifiedLocation { get; set; }

        [JsonProperty("language_list", NullValueHandling = NullValueHandling.Ignore)]
        public string LanguageList { get; set; }
    }

    public class Target
    {
        [JsonProperty("altIdentifiers", NullValueHandling = NullValueHandling.Ignore)]
        public Uri[] AltIdentifiers { get; set; }

        [JsonProperty("types", NullValueHandling = NullValueHandling.Ignore)]
        public Uri[] Types { get; set; }

        [JsonProperty("identifier", NullValueHandling = NullValueHandling.Ignore)]
        public string Identifier { get; set; }

        [JsonProperty("objectType", NullValueHandling = NullValueHandling.Ignore)]
        public ObjectType? ObjectType { get; set; }

        [JsonProperty("contentType", NullValueHandling = NullValueHandling.Ignore)]
        public ContentTypeEnum? ContentType { get; set; }

        [JsonProperty("workExpressedBy", NullValueHandling = NullValueHandling.Ignore)]
        public WorkExpressedBy[] WorkExpressedBy { get; set; }

        [JsonProperty("href", NullValueHandling = NullValueHandling.Ignore)]
        public Uri Href { get; set; }
        
        [JsonProperty("part", NullValueHandling = NullValueHandling.Ignore)]
        public Part Part { get; set; }

        [JsonProperty("title", NullValueHandling = NullValueHandling.Ignore)]
        public string Title { get; set; }

        [JsonProperty("altKeys", NullValueHandling = NullValueHandling.Ignore)]
        public string[] AltKeys { get; set; }

        [JsonProperty("language", NullValueHandling = NullValueHandling.Ignore)]
        public string Language { get; set; }

        [JsonProperty("isExpressionOfWork", NullValueHandling = NullValueHandling.Ignore)]
        public TargetIsExpressionOfWork IsExpressionOfWork { get; set; }
    }

    public class TargetIsExpressionOfWork
    {
        [JsonProperty("type", NullValueHandling = NullValueHandling.Ignore)]
        public string[] Type { get; set; }

        [JsonProperty("identifier", NullValueHandling = NullValueHandling.Ignore)]
        public string Identifier { get; set; }

        [JsonProperty("contentType", NullValueHandling = NullValueHandling.Ignore)]
        public string ContentType { get; set; }

        [JsonProperty("href", NullValueHandling = NullValueHandling.Ignore)]
        public Uri Href { get; set; }
    }

    public class TableOfContents
    {
        [JsonProperty("alt_keys", NullValueHandling = NullValueHandling.Ignore)]
        public string[] AltKeys { get; set; }

        [JsonProperty("html_title", NullValueHandling = NullValueHandling.Ignore)]
        public string HtmlTitle { get; set; }

        [JsonProperty("title", NullValueHandling = NullValueHandling.Ignore)]
        public string Title { get; set; }

        [JsonProperty("tocItems", NullValueHandling = NullValueHandling.Ignore)]
        public TocItem[] TocItems { get; set; }
    }

    public class TocItem
    {
        [JsonProperty("title", NullValueHandling = NullValueHandling.Ignore)]
        public string Title { get; set; }

        [JsonProperty("document", NullValueHandling = NullValueHandling.Ignore)]
        public Document Document { get; set; }

        [JsonProperty("position", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(ParseStringConverter))]
        public long? Position { get; set; }

        [JsonProperty("fragmented_title", NullValueHandling = NullValueHandling.Ignore)]
        public TocItemFragmentedTitle FragmentedTitle { get; set; }

        [JsonProperty("alt_keys", NullValueHandling = NullValueHandling.Ignore)]
        public string[] AltKeys { get; set; }

        [JsonProperty("children", NullValueHandling = NullValueHandling.Ignore)]
        public Child[] Children { get; set; }

        [JsonProperty("itemData", NullValueHandling = NullValueHandling.Ignore)]
        public string ItemData { get; set; }
    }

    public class Resource
    {
        [JsonProperty("format", NullValueHandling = NullValueHandling.Ignore)]
        public string Format { get; set; }

        [JsonProperty("rights", NullValueHandling = NullValueHandling.Ignore)]
        public string Rights { get; set; }

        [JsonProperty("source", NullValueHandling = NullValueHandling.Ignore)]
        public string Source { get; set; }

        [JsonProperty("altKeys", NullValueHandling = NullValueHandling.Ignore)]
        public string[] AltKeys { get; set; }

        [JsonProperty("altIdentifiers", NullValueHandling = NullValueHandling.Ignore)]
        public Uri[] AltIdentifiers { get; set; }

        [JsonProperty("contentType", NullValueHandling = NullValueHandling.Ignore)]
        public string ContentType { get; set; }

        [JsonProperty("description", NullValueHandling = NullValueHandling.Ignore)]
        public string Description { get; set; }

        [JsonProperty("href", NullValueHandling = NullValueHandling.Ignore)]
        public Uri Href { get; set; }

        [JsonProperty("identifier", NullValueHandling = NullValueHandling.Ignore)]
        public string Identifier { get; set; }

        [JsonProperty("item", NullValueHandling = NullValueHandling.Ignore)]
        public Uri Item { get; set; }
    }

    public class WorkExpressedBy
    {
        [JsonProperty("title", NullValueHandling = NullValueHandling.Ignore)]
        public string Title { get; set; }

        [JsonProperty("language", NullValueHandling = NullValueHandling.Ignore)]
        public Language? Language { get; set; }

        [JsonProperty("href", NullValueHandling = NullValueHandling.Ignore)]
        public Uri Href { get; set; }

        [JsonProperty("identifier", NullValueHandling = NullValueHandling.Ignore)]
        public string Identifier { get; set; }

        [JsonProperty("publisher", NullValueHandling = NullValueHandling.Ignore)]
        public PublisherClass Publisher { get; set; }

        [JsonProperty("isExpressionOfWork", NullValueHandling = NullValueHandling.Ignore)]
        public PublisherClass IsExpressionOfWork { get; set; }
    }

    public class PublisherClass
    {
        [JsonProperty("identifier", NullValueHandling = NullValueHandling.Ignore)]
        public string Identifier { get; set; }
    }

    public class Subject
    {
        [JsonProperty("title", NullValueHandling = NullValueHandling.Ignore)]
        public string Title { get; set; }

        [JsonProperty("altKeys", NullValueHandling = NullValueHandling.Ignore)]
        public string[] AltKeys { get; set; }

        [JsonProperty("altIdentifiers", NullValueHandling = NullValueHandling.Ignore)]
        public Uri[] AltIdentifiers { get; set; }
    }

    public class Child
    {
        [JsonProperty("title", NullValueHandling = NullValueHandling.Ignore)]
        public string Title { get; set; }

        [JsonProperty("document", NullValueHandling = NullValueHandling.Ignore)]
        public Document Document { get; set; }

        [JsonProperty("fragmented_title", NullValueHandling = NullValueHandling.Ignore)]
        public ChildFragmentedTitle FragmentedTitle { get; set; }

        [JsonProperty("position", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(ParseStringConverter))]
        public long? Position { get; set; }
        
        [JsonProperty("alt_keys", NullValueHandling = NullValueHandling.Ignore)]
        public string[] AltKeys { get; set; }


        [JsonProperty("children", NullValueHandling = NullValueHandling.Ignore)]
        public Child[] Children { get; set; }
        
        [JsonProperty("properties", NullValueHandling = NullValueHandling.Ignore)]
        public Property[] Properties { get; set; }

        [JsonProperty("itemData", NullValueHandling = NullValueHandling.Ignore)]
        public string ItemData { get; set; }
    }

    public class Document
    {
        [JsonProperty("title", NullValueHandling = NullValueHandling.Ignore)]
        public string Title { get; set; }

        [JsonProperty("altKeys", NullValueHandling = NullValueHandling.Ignore)]
        public string[] AltKeys { get; set; }

        [JsonProperty("contentType", NullValueHandling = NullValueHandling.Ignore)]
        public ContentTypeEnum? ContentType { get; set; }

        [JsonProperty("altIdentifiers", NullValueHandling = NullValueHandling.Ignore)]
        public Uri[] AltIdentifiers { get; set; }
        
        [JsonProperty("href", NullValueHandling = NullValueHandling.Ignore)]
        public Uri Href { get; set; }
    }

    public class ChildFragmentedTitle
    {
        [JsonProperty("num", NullValueHandling = NullValueHandling.Ignore)]
        public string Num { get; set; }

        [JsonProperty("type", NullValueHandling = NullValueHandling.Ignore)]
        public ContentTypeEnum? Type { get; set; }

        [JsonProperty("title", NullValueHandling = NullValueHandling.Ignore)]
        public string Title { get; set; }

        [JsonProperty("subtitle", NullValueHandling = NullValueHandling.Ignore)]
        public string Subtitle { get; set; }
    }

    public class TocItemFragmentedTitle
    {
        [JsonProperty("title", NullValueHandling = NullValueHandling.Ignore)]
        public string Title { get; set; }

        [JsonProperty("subtitle", NullValueHandling = NullValueHandling.Ignore)]
        public string Subtitle { get; set; }
    }

    public class Valid
    {
        [JsonProperty("start", NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? Start { get; set; }

        [JsonProperty("status", NullValueHandling = NullValueHandling.Ignore)]
        public string Status { get; set; }
    }

    public enum Language { De, En, Es, Fr, It, Nl };

    public enum SubRelationType { AddsTo, Completes, Replaces };

    public enum RelationType { Corrects, Comments, Adopts, AmendedBy, AmendmentProposedBy, Amends, BasedOn, BasisFor, CitedBy, Cites, CompletedBy, ConsolidatedBy, CorrectedBy, ImplementedBy, InterpretedBy, ProducedBy, Repeals, SummarizedBy };

    public enum ObjectType { DocumentWork, DocumentExpression };

    public enum ContentTypeEnum { Text, Annex, Article, Final, Preamble };

    public partial class PocModuloExpression
    {
        public static PocModuloExpression FromJson(string json) => JsonConvert.DeserializeObject<PocModuloExpression>(json, Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this PocModuloExpression self) => JsonConvert.SerializeObject(self, Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                LanguageConverter.Singleton,
                SubRelationTypeConverter.Singleton,
                RelationTypeConverter.Singleton,
                ObjectTypeConverter.Singleton,
                ContentTypeEnumConverter.Singleton,
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }

    internal class LanguageConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(Language) || t == typeof(Language?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            switch (value)
            {
                case "de":
                    return Language.De;
                case "en":
                    return Language.En;
                case "es":
                    return Language.Es;
                case "fr":
                    return Language.Fr;
                case "it":
                    return Language.It;
                case "nl":
                    return Language.Nl;
            }
            throw new Exception("Cannot unmarshal type Language");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (Language)untypedValue;
            switch (value)
            {
                case Language.De:
                    serializer.Serialize(writer, "de");
                    return;
                case Language.En:
                    serializer.Serialize(writer, "en");
                    return;
                case Language.Es:
                    serializer.Serialize(writer, "es");
                    return;
                case Language.Fr:
                    serializer.Serialize(writer, "fr");
                    return;
                case Language.It:
                    serializer.Serialize(writer, "it");
                    return;
                case Language.Nl:
                    serializer.Serialize(writer, "nl");
                    return;
            }
            throw new Exception("Cannot marshal type Language");
        }

        public static readonly LanguageConverter Singleton = new LanguageConverter();
    }

    internal class ParseStringConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(long) || t == typeof(long?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            long l;
            if (Int64.TryParse(value, out l))
            {
                return l;
            }
            throw new Exception("Cannot unmarshal type long");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (long)untypedValue;
            serializer.Serialize(writer, value.ToString());
        }

        public static readonly ParseStringConverter Singleton = new ParseStringConverter();
    }

    internal class SubRelationTypeConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(SubRelationType) || t == typeof(SubRelationType?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            switch (value)
            {
                case "adds_to":
                    return SubRelationType.AddsTo;
                case "completes":
                    return SubRelationType.Completes;
                case "replaces":
                    return SubRelationType.Replaces;
            }
            throw new Exception("Cannot unmarshal type SubRelationType");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (SubRelationType)untypedValue;
            switch (value)
            {
                case SubRelationType.AddsTo:
                    serializer.Serialize(writer, "adds_to");
                    return;
                case SubRelationType.Completes:
                    serializer.Serialize(writer, "completes");
                    return;
                case SubRelationType.Replaces:
                    serializer.Serialize(writer, "replaces");
                    return;
            }
            throw new Exception("Cannot marshal type SubRelationType");
        }

        public static readonly SubRelationTypeConverter Singleton = new SubRelationTypeConverter();
    }

    internal class RelationTypeConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(RelationType) || t == typeof(RelationType?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            switch (value)
            {
                case "corrects":
                    return RelationType.Corrects;
                case "comments":
                    return RelationType.Comments;
                case "adopts":
                    return RelationType.Adopts;
                case "amended_by":
                    return RelationType.AmendedBy;
                case "amendment_proposed_by":
                    return RelationType.AmendmentProposedBy;
                case "amends":
                    return RelationType.Amends;
                case "based_on":
                    return RelationType.BasedOn;
                case "basis_for":
                    return RelationType.BasisFor;
                case "cited_by":
                    return RelationType.CitedBy;
                case "cites":
                    return RelationType.Cites;
                case "completed_by":
                    return RelationType.CompletedBy;
                case "consolidated_by":
                    return RelationType.ConsolidatedBy;
                case "corrected_by":
                    return RelationType.CorrectedBy;
                case "implemented_by":
                    return RelationType.ImplementedBy;
                case "interpreted_by":
                    return RelationType.InterpretedBy;
                case "produced_by":
                    return RelationType.ProducedBy;
                case "repeals":
                    return RelationType.Repeals;
                case "summarized_by":
                    return RelationType.SummarizedBy;
            }
            throw new Exception("Cannot unmarshal type RelationType");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (RelationType)untypedValue;
            switch (value)
            {
                case RelationType.Corrects:
                    serializer.Serialize(writer, "corrects");
                    return;
                case RelationType.Comments:
                    serializer.Serialize(writer, "comments");
                    return;
                case RelationType.Adopts:
                    serializer.Serialize(writer, "adopts");
                    return;
                case RelationType.AmendedBy:
                    serializer.Serialize(writer, "amended_by");
                    return;
                case RelationType.AmendmentProposedBy:
                    serializer.Serialize(writer, "amendment_proposed_by");
                    return;
                case RelationType.Amends:
                    serializer.Serialize(writer, "amends");
                    return;
                case RelationType.BasedOn:
                    serializer.Serialize(writer, "based_on");
                    return;
                case RelationType.BasisFor:
                    serializer.Serialize(writer, "basis_for");
                    return;
                case RelationType.CitedBy:
                    serializer.Serialize(writer, "cited_by");
                    return;
                case RelationType.Cites:
                    serializer.Serialize(writer, "cites");
                    return;
                case RelationType.CompletedBy:
                    serializer.Serialize(writer, "completed_by");
                    return;
                case RelationType.ConsolidatedBy:
                    serializer.Serialize(writer, "consolidated_by");
                    return;
                case RelationType.CorrectedBy:
                    serializer.Serialize(writer, "corrected_by");
                    return;
                case RelationType.ImplementedBy:
                    serializer.Serialize(writer, "implemented_by");
                    return;
                case RelationType.InterpretedBy:
                    serializer.Serialize(writer, "interpreted_by");
                    return;
                case RelationType.ProducedBy:
                    serializer.Serialize(writer, "produced_by");
                    return;
                case RelationType.Repeals:
                    serializer.Serialize(writer, "repeals");
                    return;
                case RelationType.SummarizedBy:
                    serializer.Serialize(writer, "summarized_by");
                    return;
            }
            throw new Exception("Cannot marshal type RelationType");
        }

        public static readonly RelationTypeConverter Singleton = new RelationTypeConverter();
    }

    internal class ObjectTypeConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(ObjectType) || t == typeof(ObjectType?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            switch (value)
            {
                case "document_work":
                    return ObjectType.DocumentWork;
                case "document_expression":
                    return ObjectType.DocumentExpression;
                default:
                    throw new Exception("Cannot unmarshal type ObjectType");
            }
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (ObjectType)untypedValue;
            switch (value)
            {
                case ObjectType.DocumentWork:
                    serializer.Serialize(writer, "document_work");
                    return;
                case ObjectType.DocumentExpression:
                    serializer.Serialize(writer, "document_expression");
                    return;
                default:
                    throw new Exception("Cannot marshal type ObjectType");
            }
        }

        public static readonly ObjectTypeConverter Singleton = new ObjectTypeConverter();
    }

    internal class ContentTypeEnumConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(ContentTypeEnum) || t == typeof(ContentTypeEnum?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            switch (value)
            {
                case "text":
                    return ContentTypeEnum.Text;
                case "annex":
                    return ContentTypeEnum.Annex;
                case "article":
                    return ContentTypeEnum.Article;
                case "final":
                    return ContentTypeEnum.Final;
                case "preamble":
                    return ContentTypeEnum.Preamble;
            }
            throw new Exception("Cannot unmarshal type ContentTypeEnum");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (ContentTypeEnum)untypedValue;
            switch (value)
            {
                case ContentTypeEnum.Text:
                    serializer.Serialize(writer, "text");
                    return;
                case ContentTypeEnum.Annex:
                    serializer.Serialize(writer, "annex");
                    return;
                case ContentTypeEnum.Article:
                    serializer.Serialize(writer, "article");
                    return;
                case ContentTypeEnum.Final:
                    serializer.Serialize(writer, "final");
                    return;
                case ContentTypeEnum.Preamble:
                    serializer.Serialize(writer, "preamble");
                    return;
            }
            throw new Exception("Cannot marshal type ContentTypeEnum");
        }

        public static readonly ContentTypeEnumConverter Singleton = new ContentTypeEnumConverter();
    }

    /// <summary>
    /// Expression Result
    /// </summary>
    public class ExpressionResult
    {
        [JsonProperty("title", NullValueHandling = NullValueHandling.Ignore)]
        public string Title { get; set; }

        [JsonProperty("toc", NullValueHandling = NullValueHandling.Ignore)]
        public TableOfContents Toc { get; set; }

        [JsonProperty("item", NullValueHandling = NullValueHandling.Ignore)]
        public string Item { get; set; }

        [JsonProperty("relations", NullValueHandling = NullValueHandling.Ignore)]
        public Relation[] Relations { get; set; }    }
}