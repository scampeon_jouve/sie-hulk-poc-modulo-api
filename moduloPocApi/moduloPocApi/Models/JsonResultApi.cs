﻿using System.Net;
using Newtonsoft.Json;

namespace moduloPocApi.Models
{
    public class JsonResultApi
    {
        [JsonProperty("status")]
        public HttpStatusCode StatusCode;
        
        [JsonProperty("error")]
        public string Error;
        
        [JsonProperty("message")]
        public string Message;
        
        public JsonResultApi(HttpStatusCode statusCode, string error, string message)
        {
            StatusCode = statusCode;
            Error = error;
            Message = message;
        }
    }
}