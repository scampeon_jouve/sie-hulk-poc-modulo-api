﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Services.Commons.Datacontract.V2.BooleanTree;
using Services.Commons.Datacontract.V2.Query;

namespace moduloPocApi.Models
{
    [Serializable]
    [JsonObject]
    public class SearchEngineParameters
    {
        /// <summary>
        ///     Met les paramètres par défauts pour chaque critères
        /// </summary>
        public SearchEngineParameters()
        {
            CNoPage = 1;
            CNbResultats = 0;
            Request = null;
            Sorts = new List<Sort>();
            IsInitVue = false;
            RequestDescription = string.Empty;
            Metas = new string[0];
            Facets = new List<Facet>();
            CategoryLevel = null;
            SubCategories = null;
            CriteriaFilters = new List<CriterionFilter>();
            UrMarkers = new UrMarker[0];
            ValidateQueryTree = true;
        }

        /// <summary>
        ///     Requête
        /// </summary>
        [Description("Representation of the request to send to the search engine")]
        [JsonProperty("request")]
        public SearchEngineQuery Request { get; set; }

        //FIXME H2O : give more sens to this object, separation of concept, introduce sub-object to distinguish result, ... !

        #region Pilotage de la réponse, partie group

        /// <summary>
        ///     Indique s'il on veut construire les catégories Source uniquement (sans FDOC et DOMAINE)
        /// </summary>
        [Description("Reserved internal property (to be completed)")]
        [JsonProperty("reserved-categories-source-only")]
        public bool CategoriesSourceOnly { get; set; }

        /// <summary>
        ///     Indique les sous-catégories (de "source") que l'on veut récupérer ainsi que la profondeur de niveau pour chacune
        ///     d'entre elles.
        ///     Afin de réduire le volume de données récupérées en ne prenant que le nécessaire.
        /// </summary>
        [Description("Reserved internal property (to be completed)")]
        [JsonProperty("reserved-sub-categories")]
        public List<SubCategory> SubCategories { get; set; }

        /// <summary>
        ///     Liste des Refinements
        /// </summary>
        [Obsolete("H2O: Est ce bien obsolete ? : needs to be renamed")]
        [Description("Reserved internal property (report hits on document toc)")]
        [JsonProperty("refinements")]
        public Refinement[] Refinements { get; set; }

        /// <summary>
        ///     Facet names: dans le but de remplacer Zapette, Tabs, Perimeter
        /// </summary>
        [Description("Facets to add to the response")]
        [JsonProperty("facets")]
        public List<Facet> Facets { get; set; }

        /// <summary>
        ///     Liste des urMarkers
        /// </summary>
        [Description("Parameters to mark the results on condition (UrMarker)")]
        [JsonProperty("ur-markers")]
        public UrMarker[] UrMarkers { get; set; }

        #endregion

        /// <summary>
        ///     Permet d'enlever le calcul de score sur une requête
        /// </summary>
        [Description("Enable the validation of the query (true by default)")]
        [JsonProperty("validate-query-tree")]
        public bool ValidateQueryTree { get; set; }

        /// <summary>
        ///     Permet d'enlever le calcul de score sur une requête
        /// </summary>
        [Description("Remove the score computation for the query")]
        [JsonProperty("no-score")]
        public bool NoScore { get; set; }

        /// <summary>
        ///     Permet d'ajouter la Relevance part (additive) to the query
        /// </summary>
        [Description("Add the relevance additive part to the query")]
        [JsonProperty("use-relevance")]
        public bool UseRelevance { get; set; }

        /// <summary>
        ///     Listes des métas que l'on souhaite avoir dans la réponse
        /// </summary>
        [Description("Meta data list to add to the response")]
        [JsonProperty("meta-data-list")]
        public string[] Metas { get; set; }

        /// <summary>
        ///     Id du template de métas que l'on souhaite avoir dans la réponse
        /// </summary>
        [Description("Id of the template of metas to be added to the response")]
        [JsonProperty("meta-data-template-id")]
        public string TemplateMetaId { get; set; }

        /// <summary>
        ///     Le context de recherche (précedente recherche)
        /// </summary>
        [Description("Reserved internal property (to be completed)")]
        [JsonProperty("query-context")]
        public string QueryContext { get; set; }

        /// <summary>
        ///     Numéro de page
        /// </summary>
        [Description("Page number of the result list")]
        [JsonProperty("result-page-number")]
        public int CNoPage { get; set; }

        /// <summary>
        ///     Nombre de résultats souhaités
        /// </summary>
        [Description("Number of results per page")]
        [JsonProperty("result-number-per-page")]
        public int CNbResultats { get; set; }

        /// <summary>
        ///     Overriding of the default search engine (instance) relevance parameter
        /// </summary>
        [Description("Parameters to override the setted relevance (refer to the configuration) (to be completed)")]
        [JsonProperty("relevance-overriding-parameters")]
        public Dictionary<string, Dictionary<string, int>> RelevanceOverriding { get; set; }

        /// <summary>
        ///     Definition des tris à appliquer
        /// </summary>
        [Description("Sorts to apply (must be defined in the search service, refer to the configuration) to the result list")]
        [JsonProperty("sorts")]
        public List<Sort> Sorts { get; set; }

        /// <summary>
        ///     Description de la requête afin de savoir a quoi elle sert lors de la trace des performances
        /// </summary>
        [Description("Description of the request, for trace usage")]
        [JsonProperty("request-description")]
        public string RequestDescription { get; set; }

        /// <summary>
        ///     Le résumé dans
        /// </summary>
        [Description("Add the search engine computed summary in result item")]
        [JsonProperty("display-summary")]
        public bool Summary { get; set; }

        /// <summary>
        ///     Active la récupération la méta de highligting
        /// </summary>
        [Description("Add the meta highligting in result item")]
        [JsonProperty("display-highlighting")]
        public bool Highlighting { get; set; }

        /// <summary>
        ///     Active l'highligting sur les UA
        /// </summary>
        [Description("Activate the highlight on UA")]
        [JsonProperty("ua-highlighting")]
        public bool UaHighlighting { get; set; }

        /// <summary>
        ///     Active la fonctionnalité SpellCheck
        /// </summary>
        [Description("Activate the spellcheck functionnality")]
        [JsonProperty("spell-check")]
        public bool SpellCheck { get; set; }

        /// <summary>
        ///     Active la fonctionnalité AllowDebug
        /// </summary>
        [Description("Activate the debug functionnality")]
        [JsonProperty("allow-debug")]
        public bool AllowDebug { get; set; }

        /// <summary>
        ///     Should we get the URL called in the search engine
        /// </summary>
        [Description("Display the search engine request (debug purpose)")]
        [JsonProperty("display-search-engine-request")]
        public bool DisplaySearchEngineRequest { get; set; }

        /// <summary>
        ///     Permet de choisir la profondeur pour les sous catégories (quand Categories=true )
        /// </summary>
        [Description("Reserved internal property (to be completed)")]
        [JsonProperty("category-level")]
        public string CategoryLevel { get; set; }

        [Obsolete]
        [Description("Reserved internal property (to be completed)")]
        [JsonProperty("display-fdoc")]
        public bool ShowFdoc { get; set; }

        /// <summary>
        ///     True if we allow to get zero result for a request
        /// </summary>
        [Description("Enable to retrieve an empty result (with 0 hit)")]
        [JsonProperty("allow-zero-result")]
        public bool AllowZeroResult { get; set; }

        /// <summary>
        ///     Indice de départ pour le résultat
        /// </summary>
        [Description("Start indice result number (override the pagination 'result-page-number')")]
        [JsonProperty("start-number")]
        public int StartNumber { get; set; }

        /// <summary>
        ///     For compatibility purpose: Auto compute value
        /// </summary>
        [JsonIgnore]
        public int Start => (CNoPage - 1) * CNbResultats;

        [Description("Reserved internal property (to be completed)")]
        [JsonProperty("reserved-highlight-toc")]
        public bool HighlightToc { get; set; }

        [JsonIgnore]
        public static bool IsUdTypeGuidance { get; set; }

        /// <summary>
        ///     Determine si l'on est dans la vue init
        /// </summary>
        [Description("Reserved internal property (to be completed)")]
        [JsonProperty("reserved-is-init-view")]
        public bool IsInitVue { get; set; }

        /// <summary>
        ///     Permet de spécifier des filtres sur le criterion Id à appliquer avec les valeurs fournies.
        /// </summary>
        [Description("Allow to defined filters sub-query on specified criterion id")]
        [JsonProperty("criterion-filters")]
        public List<CriterionFilter> CriteriaFilters { get; set; }

        /// <summary>
        ///     Liste des SecurityTokens à passer à la requête
        /// </summary>
        [Description("Boolean tree of tokens")]
        [JsonProperty("filter-tokens")]
        public BooleanTree<string> SecurityTokens { get; set; }

        /// <summary>
        ///     Define the solr edismax parser for the query
        /// </summary>
        [Description("Define the solr edismax parser for the query")]
        [JsonProperty("edismax-parser-id")]
        public string EdismaxParserId { get; set; }

        /// <summary>
        ///     Use a specific edismax parser for the query (not the default one)
        /// </summary>
        [Description("Use a specific edismax parser for the query (not the default one)")]
        [JsonProperty("use-specific-edismax-parser")]
        public bool UseSpecificEdismaxParser { get; set; }
    }

    /// <summary>
    ///     Sens de l'ordre
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum SortOrder
    {
        [EnumMember(Value = "asc")] Asc,
        [EnumMember(Value = "desc")] Desc
    }

    /// <summary>
    ///     Définit une sous-catégorie et le niveau de profondeur de données souhaité
    /// </summary>
    [Serializable]
    [JsonObject]
    public class SubCategory
    {
        public SubCategory()
        {
            CategoryName = string.Empty;
            CategoryLevel = string.Empty;
        }

        [Description("Reserved internal property (to be completed)")]
        [JsonProperty("category-name")]
        public string CategoryName { get; set; }

        [Description("Reserved internal property (to be completed)")]
        [JsonProperty("category-level")]
        public string CategoryLevel { get; set; }
    }


    /// <summary>
    ///     Définit un tri
    /// </summary>
    [Serializable]
    [JsonObject]
    public class Sort
    {
        public Sort()
        {
            Id = string.Empty;
            Order = SortOrder.Desc;
        }

        [Description("Sort id used to perform the sort of the result (refer to the configuration)")]
        [JsonProperty("sort-id")]
        public string Id { get; set; }

        [Description("Sort order used: \"asc\" or \"desc\"")]
        [JsonProperty("sort-order")]
        public SortOrder Order { get; set; }
    }

    /// <summary>
    ///     Définit une facet
    /// </summary>
    [Serializable]
    [JsonObject]
    public class Facet
    {
        public Facet()
        {
            Name = string.Empty;
            Sort = string.Empty;
        }

        [Description("facet name used")]
        [JsonProperty("facet-name")]
        public string Name { get; set; }

        [Description("facet sort used: index(alphabetical) or count")]
        [JsonProperty("facet-sort")]
        public string Sort { get; set; }
    }

    public class UrMarker
    {
        [Description("A value to 'mark' the result with")]
        [JsonProperty("marker")]
        public string Marker { get; set; }

        [Description("The score number until the result are not marked")]
        [JsonProperty("relevance-above")]
        public long RelevanceAbove { get; set; }

        [Description("Number of results to mark that have a score superior to 'RelevanceAbove'")]
        [JsonProperty("max-marked")]
        public int MaxMarked { get; set; }
    }

    [Serializable]
    [JsonObject]
    public class Refinement
    {
        #region Constructor

        public Refinement() { }

        public Refinement(string refine, bool excluded, string id)
        {
            RefineId = refine;
            Excluded = excluded;
            Id = id;
        }

        #endregion

        #region Properties

        [Description("A value to refine the result (to be completed)")]
        [JsonProperty("refinement-id")]
        public string RefineId { get; set; }

        [Description("Id of the criterion used to refine the result (to be completed)")]
        [JsonProperty("criterion-id")]
        public string Id { get; set; }

        [Description("If setted to true, the filter become a negation (NOT filter) (to be completed)")]
        [JsonProperty("is-excluded")]
        public bool Excluded { get; set; }

        #endregion

        #region Equality

        protected bool Equals(Refinement other)
        {
            return string.Equals(RefineId, other.RefineId) && Excluded.Equals(other.Excluded);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
                return false;
            if (ReferenceEquals(this, obj))
                return true;
            if (obj.GetType() != GetType())
                return false;
            return Equals((Refinement) obj);
        }

        public override int GetHashCode()
        {
            unchecked {
                return ((RefineId != null ? RefineId.GetHashCode() : 0) * 397) ^ Excluded.GetHashCode();
            }
        }

        #endregion

        #region ToString

        public override string ToString()
        {
            return string.Concat(Excluded ? '-' : '+', RefineId);
        }

        #endregion
    }
}