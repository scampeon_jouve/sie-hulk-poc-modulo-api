﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using moduloPocApi.Managers;
using moduloPocApi.Models;
using moduloPocApi.Models.Entities;
using moduloPocApi.Services;
using moduloPocApi.Utils;

namespace moduloPocApi.Providers
{
    public class DataProviderClient : ServiceHttpClient
    {
        private ResourceTransformer ResourceTransformer { get; }
        private List<string> Formats { get; }

        public DataProviderClient(string serviceUrl) : base(serviceUrl)
        {
            Formats = new List<string> {"text/xml", "text/html", "application/xhtml+xml"};
            ResourceTransformer = new ResourceTransformer();
        }

        // pour faire les appel depuis le content alert service on utilise systématiquement le Proxy
        protected override HttpClient GetHttpClient(bool gzipEnabled = true)
        {
            return HulkHttpClient.Build(gzipEnabled, null, true);
        }

        /// <summary>
        /// Get raw data content from the data provider
        /// </summary>
        /// <returns>data as string</returns>
        public async Task<string> GetRawContent(string url = null)
        {
            try {
                var content = await GetServiceRequest<string>(new UriBuilder(url ?? ServiceUrl)).ConfigureAwait(false);

                var pocModuloExpression = PocModuloExpression.FromJson(content);
                if (pocModuloExpression.Resources != null)
                {
                    var itemUris = (from resource in pocModuloExpression.Resources
                        where !string.IsNullOrEmpty(resource.Format) && Formats.Contains(resource.Format)
                        select resource.Item);

                    foreach (var itemUri in itemUris)
                    {
                        if (itemUri.AbsoluteUri.Contains("contentrepostorage-files"))
                            return await GetServiceRequest<string>(new UriBuilder(itemUri)).ConfigureAwait(false);
                    }
                }

                return string.Empty;
            }
            catch (Exception e) {
                Console.WriteLine(e);
                new ElmahHulkLogger().Error(e);
                throw;
            }
        }

        /// <summary>
        /// Get data content from the data provider
        /// </summary>
        /// <returns>data as string</returns>
        public async Task<ExpressionResult> GetContent(string url = null)
        {
            try {
                var expressionResult = new ExpressionResult();
                var content = await GetServiceRequest<string>(new UriBuilder(url ?? ServiceUrl)).ConfigureAwait(false);

                var pocModuloExpression = PocModuloExpression.FromJson(content);

                expressionResult.Title = pocModuloExpression.Title;
                expressionResult.Toc = pocModuloExpression.TableOfContents;
                expressionResult.Relations = pocModuloExpression.Relations;

                if (pocModuloExpression.Resources != null)
                {
                    var itemUris = GetItem(pocModuloExpression);

                    foreach (var itemUri in itemUris)
                    {
                        if (!itemUri.AbsoluteUri.Contains("contentrepostorage-files"))
                            continue;
                        var rawItemContent = await GetServiceRequest<string>(new UriBuilder(itemUri)).ConfigureAwait(false);
                        if (CacheUtils.Contains(itemUri.AbsoluteUri))
                        {
                            expressionResult.Item = (string) CacheUtils.Get(itemUri.AbsoluteUri);
                        }
                        else
                        {
                            expressionResult.Item = ResourceTransformer.ProcessResource(rawItemContent, pocModuloExpression.Relations);
                            CacheUtils.Add(itemUri.AbsoluteUri, expressionResult.Item);
                        }
                    }
                }

                if (expressionResult.Toc != null)
                    expressionResult.Toc = await GetTocItemData(expressionResult.Toc);

                return expressionResult;
            }
            catch (Exception e) {
                Console.WriteLine(e);
                new ElmahHulkLogger().Error(e);
                throw;
            }
        }

        public async Task<List<PublisherEntity>> GetPublishers()
        {
            var content = await GetServiceRequest<string>(new UriBuilder(ServiceUrl)).ConfigureAwait(false);
            return PublisherEntity.FromJson(content);
        }

        private async Task<TableOfContents> GetTocItemData(TableOfContents tableOfContents)
        {
            foreach (var tocItem in tableOfContents.TocItems)
            {
                if (tocItem.Document != null && tocItem.Document.Href != null)
                {
                    var tocItemExpressionResult = await GetContent(tocItem.Document.Href.AbsoluteUri);
                    if (tocItemExpressionResult?.Item != null)
                    {
                        if (CacheUtils.Contains(tocItem.Document.Href.AbsoluteUri))
                        {
                            tocItem.ItemData = (string) CacheUtils.Get(tocItem.Document.Href.AbsoluteUri);
                        }
                        else
                        {
                            tocItem.ItemData = ResourceTransformer.ProcessResource(tocItemExpressionResult.Item, tocItemExpressionResult.Relations);
                            CacheUtils.Add(tocItem.Document.Href.AbsoluteUri, tocItem.ItemData);
                        }
                    }
                }

                if (tocItem.Children != null)
                    tocItem.Children = await GetTocItemChildren(tocItem.Children);
            }

            return tableOfContents;
        }

        private async Task<Child[]> GetTocItemChildren(Child[] children)
        {
            foreach (var tocItemChild in children)
            {
                if (tocItemChild.Document != null && tocItemChild.Document.Href != null)
                {
                    var tocItemExpressionResult = await GetContent(tocItemChild.Document.Href.AbsoluteUri);
                    if (tocItemExpressionResult?.Item != null)
                    {
                        if (CacheUtils.Contains(tocItemChild.Document.Href.AbsoluteUri))
                        {
                            tocItemChild.ItemData = (string) CacheUtils.Get(tocItemChild.Document.Href.AbsoluteUri);
                        }
                        else
                        {
                            tocItemChild.ItemData = ResourceTransformer.ProcessResource(tocItemExpressionResult.Item, tocItemExpressionResult.Relations);
                            CacheUtils.Add(tocItemChild.Document.Href.AbsoluteUri, tocItemChild.ItemData);
                        }
                    }
                }

                if (tocItemChild.Children != null)
                    tocItemChild.Children = await GetTocItemChildren(tocItemChild.Children);
            }

            return children;
        }

        private IEnumerable<Uri> GetItem(PocModuloExpression pocModuloExpression)
        {
            var itemUris = from resource in pocModuloExpression.Resources
                where !string.IsNullOrEmpty(resource.Format) && Formats.Contains(resource.Format)
                select resource.Item;
            return itemUris;
        }

    }
}