﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using moduloPocApi.Attributes;
using moduloPocApi.Managers;
using moduloPocApi.Utils;
using NSwag.Annotations;

namespace moduloPocApi.Controllers
{
    /// <summary>
    ///     Represents test controller that should be removed.
    /// </summary>
    [AllowCors]
    public class DocumentController : ApiController
    {
        private DocumentServiceManager _documentServiceManager { get; }
        private ResourceTransformer _resourceTransformer { get; }

        public DocumentController()
        {
            _documentServiceManager = new DocumentServiceManager();
            _resourceTransformer = new ResourceTransformer();
        }

        [HttpPost]
        [SwaggerResponse(200, typeof(string), Description = "Success")]
        [SwaggerResponse(400, typeof(string), Description = "Bad request, maybe the json query has a wrong format")]
        [SwaggerResponse(404, typeof(string), Description = "Not found, maybe the instance for the input ids does not exist")]
        [SwaggerResponse(500, typeof(string), Description = "Internal error, maybe an exception occurs during the processing")]
        [Route("api/document/process/")]
        public IHttpActionResult ProcessResource(string rawContent)
        {
            try {
                return Ok(_resourceTransformer.ProcessResource(rawContent));
            }
            catch (Exception e) {
                new ElmahHulkLogger().Error(e);
                return InternalServerError(e);
            }
        }

        /// <summary>
        ///     Get raw Item of a document expression for publisher EFL
        /// </summary>
        /// <param name="id">Expression id</param>
        /// <returns></returns>
        [HttpGet]
        [SwaggerResponse(200, typeof(string), Description = "Success")]
        [SwaggerResponse(400, typeof(string), Description = "Bad request, maybe the json query has a wrong format")]
        [SwaggerResponse(404, typeof(string), Description = "Not found, maybe the instance for the input ids does not exist")]
        [SwaggerResponse(500, typeof(string), Description = "Internal error, maybe an exception occurs during the processing")]
        [Route("api/documents/raw/{id}")]
        public async Task<IHttpActionResult> RawDocumentEfl(string id)
        {
            var url = $"{CommonConfigUtil.ContentRepoUrl}/publishers/efl/documents/{id}/languages/fr";
            var result = await _documentServiceManager.GetDocumentItem(url);
            return Ok(result);
        }

        /// <summary>
        ///     Getting all publishers
        /// </summary>
        /// <returns>The publishers</returns>
        [HttpGet]
        [SwaggerResponse(200, typeof(string), Description = "Success")]
        [SwaggerResponse(400, typeof(string), Description = "Bad request, maybe the json query has a wrong format")]
        [SwaggerResponse(404, typeof(string), Description = "Not found, maybe the instance for the input ids does not exist")]
        [SwaggerResponse(500, typeof(string), Description = "Internal error, maybe an exception occurs during the processing")]
        [Route("api/publishers")]
        public async Task<IHttpActionResult> Publishers()
        {
            var url = $"{CommonConfigUtil.ContentRepoUrl}/publishers";
            try {
                var expressionResult = await _documentServiceManager.GetPublishers(url);
                return Ok(expressionResult);
            }
            catch (Exception e) {
                new ElmahHulkLogger().Error(e);
                return InternalServerError(e);
            }
        }

        /// <summary>
        ///     Get Item and metas of a document expression for publisher EFL
        /// </summary>
        /// <param name="id">Expression id</param>
        /// <returns></returns>
        [HttpGet]
        [SwaggerResponse(200, typeof(string), Description = "Success")]
        [SwaggerResponse(400, typeof(string), Description = "Bad request, maybe the json query has a wrong format")]
        [SwaggerResponse(404, typeof(string), Description = "Not found, maybe the instance for the input ids does not exist")]
        [SwaggerResponse(500, typeof(string), Description = "Internal error, maybe an exception occurs during the processing")]
        [Route("api/documents/{id}")]
        public async Task<IHttpActionResult> DocumentEfl(string id)
        {
            var url = $"{CommonConfigUtil.ContentRepoUrl}/publishers/efl/documents/{id}/languages/fr";

             try {
                var expressionResult = await _documentServiceManager.GetDocument(url);
                return Ok(expressionResult);
            }
            catch (Exception e) {
                new ElmahHulkLogger().Error(e);
                return InternalServerError(e);
            }
        }

        /// <summary>
        /// Get Item and metas of a document expression
        /// </summary>
        /// <param name="id">Expression id as "efl:f861c2451-b1b3-4998-b50d-51d2322ff8bb"</param>
        /// <returns></returns>
        [HttpGet]
        [SwaggerResponse(200, typeof(string), Description = "Success")]
        [SwaggerResponse(400, typeof(string), Description = "Bad request, maybe the json query has a wrong format")]
        [SwaggerResponse(404, typeof(string), Description = "Not found, maybe the instance for the input ids does not exist")]
        [SwaggerResponse(500, typeof(string), Description = "Internal error, maybe an exception occurs during the processing")]
        [Route("api/documents")]
        public async Task<IHttpActionResult> Document([FromUri] string id)
        {
            if (string.IsNullOrEmpty(id))
                return BadRequest("Missing parameter id");

            var idPart = id.Split(':');
            if (idPart.Length != 2)
                return BadRequest($"Bad parameter id value: {id}");

            var url = $"{CommonConfigUtil.ContentRepoUrl}/publishers/{idPart[0]}/documents/{idPart[1]}/languages/fr";

            try {
                var expressionResult = await _documentServiceManager.GetDocument(url);
                return Ok(expressionResult);
            }
            catch (Exception e) {
                new ElmahHulkLogger().Error(e);
                return InternalServerError(e);
            }
        }

        /// <summary>
        ///     Get raw Item of a document expression
        /// </summary>
        /// <param name="id">Expression id</param>
        /// <param name="publisher">Publisher (efl, el, dalloz, ...)</param>
        /// <returns></returns>
        [HttpGet]
        [SwaggerResponse(200, typeof(string), Description = "Success")]
        [SwaggerResponse(400, typeof(string), Description = "Bad request, maybe the json query has a wrong format")]
        [SwaggerResponse(404, typeof(string), Description = "Not found, maybe the instance for the input ids does not exist")]
        [SwaggerResponse(500, typeof(string), Description = "Internal error, maybe an exception occurs during the processing")]
        [Route("api/documents/raw/{publisher}/{id}")]
        public async Task<IHttpActionResult> RawDocument(string id, string publisher = "efl")
        {
            var url = $"{CommonConfigUtil.ContentRepoUrl}/publishers/{publisher}/documents/{id}/languages/fr";
            var result = await _documentServiceManager.GetDocumentItem(url);
            return Ok(result);
        }

        /// <summary>
        ///     Get Item and metas of a document expression
        /// </summary>
        /// <param name="id">Expression id</param>
        /// <param name="publisher">Publisher (efl, el, dalloz, ...)</param>
        /// <returns></returns>
        [HttpGet]
        [SwaggerResponse(200, typeof(string), Description = "Success")]
        [SwaggerResponse(400, typeof(string), Description = "Bad request, maybe the json query has a wrong format")]
        [SwaggerResponse(404, typeof(string), Description = "Not found, maybe the instance for the input ids does not exist")]
        [SwaggerResponse(500, typeof(string), Description = "Internal error, maybe an exception occurs during the processing")]
        [Route("api/documents/{publisher}/{id}")]
        public async Task<IHttpActionResult> Document(string id, string publisher = "efl")
        {
            var url = $"{CommonConfigUtil.ContentRepoUrl}/publishers/{publisher}/documents/{id}/languages/fr";

            try {
                var expressionResult = await _documentServiceManager.GetDocument(url);
                return Ok(expressionResult);
            }
            catch (Exception e) {
                new ElmahHulkLogger().Error(e);
                return InternalServerError(e);
            }
        }
    }
}