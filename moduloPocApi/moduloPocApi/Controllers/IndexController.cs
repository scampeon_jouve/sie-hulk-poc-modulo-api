﻿using System.Web.Mvc;

namespace moduloPocApi.Controllers
{
    /// <summary>
    ///     Default controller
    /// </summary>
    public class IndexController : Controller
    {
        /// <summary>
        ///     Redierct to the swagger Home page
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return Redirect("/swagger/");
        }
    }
}