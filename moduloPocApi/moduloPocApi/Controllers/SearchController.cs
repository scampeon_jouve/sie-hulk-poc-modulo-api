﻿using System.Web.Http;
using moduloPocApi.Attributes;
using moduloPocApi.Managers;
using moduloPocApi.Models;
using NSwag.Annotations;
using Search.Model.V2.Domain.HulkDataModel;
using Services.Commons.Datacontract.V2.Query;

namespace moduloPocApi.Controllers
{
    [AllowCors]
    public class SearchController : ApiController
    {
        private SearchManager _searchManager { get; }

        public SearchController()
        {
            _searchManager = new SearchManager();
        }


        [HttpGet]
        [SwaggerResponse(200, typeof(string), Description = "Success")]
        [SwaggerResponse(400, typeof(string), Description = "Bad request, maybe the json query has a wrong format")]
        [SwaggerResponse(404, typeof(string), Description = "Not found, maybe the instance for the input ids does not exist")]
        [SwaggerResponse(500, typeof(string), Description = "Internal error, maybe an exception occurs during the processing")]
        [Route("api/search/instance/new")]
        public void TryCreateConfiguredInstance(string id = null, string file = null)
        {
            _searchManager.TryCreateConfiguredInstance(id, file);
        }

        /// <summary>
        /// </summary>
        /// <param name="q"></param>
        /// <param name="instanceId"></param>
        /// <returns></returns>
        /// <example>api/search?q=ma+recherche+plein+texte</example>
        [HttpPost]
        [SwaggerResponse(200, typeof(string), Description = "Success")]
        [SwaggerResponse(400, typeof(string), Description = "Bad request, maybe the json query has a wrong format")]
        [SwaggerResponse(404, typeof(string), Description = "Not found, maybe the instance for the input ids does not exist")]
        [SwaggerResponse(500, typeof(string), Description = "Internal error, maybe an exception occurs during the processing")]
        [Route("api/search")]
        public IHttpActionResult Search(string q, string instanceId = "MODULO")
        {
            // Prepare the content request
            var query = new SearchEngineParameters {
                CNbResultats = 20,
                CNoPage = 1,
                Request = new SearchEngineQuery {
                    Tree = new QueryTree {
                        FullText = new FullTextQuery(q)
                    }
                },
                AllowZeroResult = true,
                DisplaySearchEngineRequest = true,
                Summary = true,
                Metas = new[] {
                    SpecificSearchMetaName.UrInfos,
                    SpecificSearchMetaName.UrId,
                    SpecificSearchMetaName.Uri,
                    SpecificSearchMetaName.TargetSgmlId,
                    SpecificSearchMetaName.UrInfosTiny,
                    SpecificSearchMetaName.UrMetas,
                    // Content Repo document metas
                    "identifier",
                    "altIdentifier",
                    "altKeys",
                    "title",
                    "types",
                    "abstract",
                    "publisherId",
                    "objectType",
                    "displayTitle",
                    "documentDate",
                    "documentNumber",
                    "legalAreas",
                    "audiences",
                    "keywords",
                    "parts",
                    "partOf",
                    "tocId",
                    "createdAt",
                    "modifiedAt",
                    "alternatives",
                    "numbers",
                    "subjects",
                    "dates"
                },
                UseRelevance = true,
                Highlighting = true,
                RequestDescription = "Poc module simple search"
            };

            // Post the data to the Search API
            return Ok(_searchManager.Search(instanceId, query));
        }
    }
}