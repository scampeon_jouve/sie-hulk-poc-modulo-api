<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="3.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:j="http://www.w3.org/2013/XSL/json"
	xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
	xmlns:xhtml="http://www.w3.org/1999/xhtml"
	xmlns:ea="http://www.lefebvre-sarrut.eu/ns/els/content/efl-actu"
	exclude-result-prefixes="xs xf xhtml ea j">

  <xsl:output method="xml" indent="no" omit-xml-declaration="yes"/>

  <!-- DOCUMENT -->
  <xsl:template match="/">
    <xsl:choose>
      <xsl:when test="xf:contentNode/xf:content">
        <xsl:apply-templates select="xf:contentNode/xf:content[1]"/>
      </xsl:when>
      <xsl:when test="xhtml:html/xhtml:body">
        <xsl:apply-templates select="xhtml:html/xhtml:body[1]"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy-of select="."/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="xf:content | xhtml:body">
    <section>
      <xsl:apply-templates select="node()"/>
    </section>
  </xsl:template>

  <!-- LINKS to documents -->
  <xsl:template match="ea:a[@xf:targetResId]">
    <xsl:variable name="l_target" select="document(concat('cr:',@xf:targetResId))"/>
    <a data-target-id="{$l_target/target/@id}" data-target-title="{$l_target/target/@title}" data-target-type="{$l_target/target/@content-type}" data-content-type="{$l_target/target/@relation-type}" data-anchor="{@xf:idRef}">
      <xsl:copy-of select="@* except @xf:*"/>
      <xsl:apply-templates select="node()"/>
    </a>
  </xsl:template>

  <!-- LINKS to videos -->
  <xsl:template match="ea:video[@data-els-src-platform='youtube']">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/{@data-els-src-id}" frameborder="0"
			allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="">
      <xsl:comment>empty</xsl:comment>
    </iframe>
  </xsl:template>

  <!-- TAGS TO DELETE -->
  <xsl:template match="*:script"/>

  <!-- COPY ALL TREE without namespaces (local-name) -->
  <xsl:template match="element()">
    <xsl:element name="{local-name()}">
      <xsl:copy-of select="@*"/>
      <xsl:apply-templates select="node()"/>
    </xsl:element>
  </xsl:template>

</xsl:stylesheet>