﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Cors;
using System.Web.Http.Cors;
using moduloPocApi.Utils;

namespace moduloPocApi.Attributes
{
    /// <summary>
    ///     Attribute for custom CORS configuration
    /// </summary>
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class)]
    public class AllowCorsAttribute : Attribute, ICorsPolicyProvider
    {
        private readonly CorsPolicy _policy;

        /// <summary>
        ///     Create a CORS policy from the appsettings configuration file
        /// </summary>
        public AllowCorsAttribute()
        {
            // Create a CORS policy.
            _policy = new CorsPolicy {
                AllowAnyMethod = true,
                AllowAnyHeader = true,
                SupportsCredentials = true
            };

            // Add allowed origins. 
            var origins = CommonConfigUtil.AllowedOrigins;
            if (string.IsNullOrEmpty(origins))
                return;

            if ("*".Equals(origins))
                _policy.AllowAnyOrigin = true;

            foreach (var origin in origins.Split(',').ToList())
                _policy.Origins.Add(origin);
        }

        /// <summary>
        ///     Get the CORS policy
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns>the CORS policy</returns>
        public Task<CorsPolicy> GetCorsPolicyAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_policy);
        }
    }
}