﻿using System.IO;
using System.Linq;
using System.Reflection;

namespace moduloPocApiTests.Misc
{
    public class TestFilesHelper
    {
        /// <summary>
        ///     If the file does not exist at the root of output directory of the UNIT TEST, we search in the content of the
        ///     current directory
        ///     where we expect to have the configuration file, whatever the structutre of this directory
        /// </summary>
        /// <param name="relativePath">the relative path of the configuration file</param>
        /// <returns>the relative path of the configuration file after directory browsing</returns>
        public static string TryGetLocalOrUnitTestResources(string relativePath)
        {
            return File.Exists(relativePath)
                           ? relativePath
                           : Directory.GetFiles(@".\", relativePath, SearchOption.AllDirectories).First();
        }

        /// <summary>
        ///     Build the absolute path of a file
        /// </summary>
        /// <param name="fileReference">the file name</param>
        /// <returns>the absolute path of a file</returns>
        public static string BuildAbsolutePathBasedOnCurrentDirectory(string fileReference)
        {
            if (Path.IsPathRooted(fileReference))
                return fileReference;
            var directoryName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            return directoryName == null ? fileReference : Path.Combine(directoryName, fileReference);
        }
    }
}