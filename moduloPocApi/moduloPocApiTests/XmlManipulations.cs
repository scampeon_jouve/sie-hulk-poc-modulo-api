﻿using System;
using System.IO;
using System.Xml;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using moduloPocApiTests.Misc;
using Saxon.Api;

namespace moduloPocApiTests
{
    [TestClass]
    [DeploymentItem("TestFiles\\", "")]
    public class XmlManipulations
    {
        [TestMethod]
        public void ProcessXmlWithXsl()
        {
            var xmlFile = TestFilesHelper.TryGetLocalOrUnitTestResources("xml-content-to-xhtml.xml");
            var xmlFileContent = File.ReadAllText(xmlFile);

            var xslFile = TestFilesHelper.TryGetLocalOrUnitTestResources("xml-content-to-xhtml.xsl");
            var xslFileContent = File.ReadAllText(xslFile);

            // Create a Processor instance.
            var processor = new Processor();

            // Load the source document.
            var input = processor.NewDocumentBuilder().Build(XmlReader.Create(new StringReader(xmlFileContent)));

            var compiler = processor.NewXsltCompiler();

            // Not required but we need to set otherwise exception will be thrown
            compiler.BaseUri = new Uri("http://dummyurl.com");

            // Create a transformer for the stylesheet.
            var transformer = compiler.Compile(XmlReader.Create(new StringReader(xslFileContent))).Load();
            transformer.InitialContextNode = input;

            // Create a serializer.
            var serializer = processor.NewSerializer();
            serializer.SetOutputProperty(Serializer.INDENT, "yes");

            // Create a serializer
            const string xmlProcessed = "xml-content-to-xhtml-processed.xml";
            var fsXmlProcessed = new FileStream(xmlProcessed, FileMode.Create, FileAccess.Write);
            serializer.SetOutputStream(fsXmlProcessed);

            // Transform the source XML to System.out.
            transformer.Run(serializer);
            fsXmlProcessed.Close();

            var xmlFileExpected = TestFilesHelper.TryGetLocalOrUnitTestResources("xml-content-to-xhtml-expected.xml");
            var xmlFileContentExpected = File.ReadAllText(xmlFileExpected);

            var fileContents = File.ReadAllText(fsXmlProcessed.Name);

            var xmlFileContentExpectedCleaned = xmlFileContentExpected.Replace("\n", "").Replace("\r", "");
            var fileContentsCleaned = fileContents.Replace("\n", "").Replace("\r", "");

            Assert.IsTrue(xmlFileContentExpectedCleaned.Equals(fileContentsCleaned));

            File.Delete(xmlProcessed);
        }
    }
}