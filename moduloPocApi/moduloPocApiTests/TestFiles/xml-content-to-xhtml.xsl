<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
  xmlns:ea="http://www.lefebvre-sarrut.eu/ns/els/content/efl-actu"
	exclude-result-prefixes="xs xf ea">
	
	<xsl:output method="xml" indent="yes" />

	<xsl:template match="/">
		<xsl:apply-templates select="xf:contentNode/xf:content"/>
	</xsl:template>

  <xsl:template match="xf:content">
    <section>
      <xsl:apply-templates select="*"/>
    </section>
  </xsl:template>

  <xsl:template match="ea:video[@data-els-src-platform='youtube']">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/{@data-els-src-id}" frameborder="0"
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="">
    </iframe>

  </xsl:template>

	<xsl:template match="element()">
		<xsl:element name="{local-name()}">
			<xsl:copy-of select="@*"/>
			<xsl:apply-templates select="node()"/>
		</xsl:element>
	</xsl:template>
	
</xsl:stylesheet>
